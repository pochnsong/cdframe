/***
 * cdframe 单页面应用框架
 *
 * 依赖jquery.js
 *
 *
 */


(function () {
    console.log('run');
    var FnQueue = {
        queue: [],
        lock: false,
        next: function () {
            while (FnQueue.queue.length > 0) {
                obj = FnQueue.queue.shift();
                var _fn = obj[0], _para = obj[1], _lock = obj[2], _async = obj[3];
                FnQueue.lock = true;
                console.log("运行 "+_lock);

                try {
                    _fn(_para);
                } catch (e) {
                    console.error("函数错误" + _lock+e)
                    FnQueue.lock = false;
                    console.log("运行结束")
                }
                if (_async) {
                    return;
                }
            }
            FnQueue.lock = false;
            console.log("运行结束")
        },
        call: function (fn, para, process, async) {
            async = async?true:false;
            console.log("加入队列", process, FnQueue.queue, async)
            FnQueue.queue.push([fn, para, process, async]);

            if (FnQueue.lock) { return }
            FnQueue.next()
        },
    }
    var Http = {
        get: function (url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.responseType = "text";
            xhr.setRequestHeader('If-Modified-Since', '0');

            xhr.onload = function () {
                if (this.status == 200) {
                    callback(this.response);
                } else {
                    callback(this.response);
                }
            }
            xhr.send();
        },
    }
    var Compiler = {
        js:function(code, mod_name, flag, page_id, root_name){
            //加载js
            var l = mod_name.length-root_name.length
            if(root_name+code.slice(0, l)!=mod_name){
                console.error("加载JS错误 mod_name不配配 "+mod_name+':'+root_name+code.slice(0, l))
                return
            }
            var mod_flag = flag+'_'+mod_name;
            //var js_code = flag+'_'+code+";"+mod_flag+'.init('+mod_flag+',"'+page_id+'");';
            var js_code = flag+'_'+root_name+code;
            eval(js_code); //初始化js
            return eval(mod_flag);
        }
    }
    function attr(obj, name, default_value) {
        if (obj == undefined) return default_value;

        if (typeof name == 'string') {
            name = [name]
        }

        var res = obj;
        for (var k in name) {
            res = res[name[k]]
            if (res === undefined) {
                res = default_value
                break
            }
        }
        return res
    }

    function get_pages(cfg, page_path, root) {
        root=root?root:[];
        var res = {}
        var k, o, i;
        var pages = cfg.pages;
        var animation;
        var root_path = root.join("/");
        if(root_path){root_path+='/'}
        var root_name = root.join("_");
        if(root_name){root_name+='_'}

        for (k in pages) {
            o = pages[k];
            animation = attr(o, 'animation', 'left');
            if(['left','right', 'top', null].indexOf(animation)<0){
                animation = 'left';
            }
            res[root_name+k] = {
                'name': root_name + k,
                'load': null,
                'root_name': root_name,
                'settings': {
                    'html': attr(o, 'html', page_path +root_path+k + '/page.html'),
                    'js': attr(o, 'js', page_path +root_path+ k + '/page.js'),
                    'css': attr(o, 'css', page_path +root_path+ k + '/page.css'),
                    'lazy': o.lazy ? true : false,
                    'cache': o.cache ? true : false,
                    'animation': animation,
                },
            }
            if(o.pages){
               var _root = JSON.parse(JSON.stringify(root));
               _root.push(k)
               var _res = get_pages(o, page_path, _root)
               for(i in _res){
                   res[i] = _res[i];
               }
            }

        }
        console.log(res)
        //alert(JSON.stringify(res))
        return res
    }
    function init_frame(el) {
        var width = el[0].style.width;
        var height = el[0].style.height;

        if(!width){width='100vw'}
        if(!height){height='100vh'}

        console.log('el height:'+ height)
        console.log('el width:'+ width)

        var _style = document.createElement("style");
        _style.type="text/css";
        _style.innerHTML = ".pageX{height:100%; width:100%;background-color:#fff;position: absolute; left:0;top:0; overflow-x:hidden;z-index:-1;}";
        //_style.innerHTML += ".pageX.left,.pageX.null{top:0;left:100%}";
        //_style.innerHTML += ".pageX.right{top:0;left:-100%}";
        //_style.innerHTML += ".pageX.top{top:-100%;left:0}";
        _style.innerHTML += "@keyframes pageXShowleft{from {left: 100%;}to {left: 0;}}";
        _style.innerHTML += "@keyframes pageXBackleft{from {left: 0;}to {left: 100%;}}";
        _style.innerHTML += "@keyframes pageXShowright{from {left: -100%}to {left: 0;}}";
        _style.innerHTML += "@keyframes pageXBackright{from {left: 0;}to {left: -100%;}}";
        _style.innerHTML += "@keyframes pageXShowtop{from {top: -100%}to {top: 0;}}";
        _style.innerHTML += "@keyframes pageXBacktop{from {top: 0;}to {top: -100%;}}";

        document.getElementsByTagName("head")[0].appendChild(_style);

        el.css('width', width).css('height', height)
        el.css('overflow', "hidden")
        el.css('position', "relative")
    }
    function get_settings(cfg) {
        return {
            'page_path': attr(cfg, 'page_path', 'templates/'),
            'page_start': attr(cfg, 'page_start', 'index'),
            'onLoaded': attr(cfg, 'onLoaded', function(){console.log("onLoaded")}),
        }
    }


    window.CDFrame = function CDFrame(el, config) {
        this.version = 20200229;
        var self = this;
        self.lock = false; //事件锁定
        this.G = {};
        this.getG=function(key){
            var value = self.G[key];
            if(value){
                delete self.G[key];
            }
            return value
        }


        this.app = $(el);
        this.history = [];
        this.page_cache={};
        this.current=null; //当前页面js对象
        init_frame(this.app);

        this.settings = get_settings(config.settings)
        this.pages = get_pages(config, this.settings.page_path);

        function load_template(name, callback, para) {
            console.log('load page fn', name);
            if(self.pages[name]['load']!=null)return;
            self.pages[name]['load'] = 'loading';

            FnQueue.call(function (name) {
                var url = self.pages[name].settings.html;
                Http.get(url, function (data) {
                    self.pages[name]['html'] = data;
                    FnQueue.next()
                })
            }, name, 'load html:' + name, true)
            FnQueue.call(function (name) {
                var url = self.pages[name].settings.js;
                Http.get(url, function (data) {
                    self.pages[name]['js'] = data
                    FnQueue.next()
                })
            }, name, 'load js:' + name, true)

            FnQueue.call(function (name) {
                var url = self.pages[name].settings.css;
                Http.get(url, function (data) {
                    self.pages[name]['css'] = data
                    FnQueue.next()
                })
            }, name, 'load css:' + name, true)

            FnQueue.call(function (name) {
                self.pages[name]['load'] = 'done';
            }, name, 'loaded page:' + name)

            if(callback){
                FnQueue.call(callback, para, 'load callback ' + name, false)
            }
        }

        function destroy_page(page_id){
            //销毁页面
            console.log('destroy', page_id);
            if(page_id in self.page_cache){
                var page_info = self.page_cache[page_id];
                delete self.page_cache[page_id]
                //var page_dom = document.getElementById(page_id);
                $("#"+page_id).remove();//清除dom
                $("#"+page_id+'_style').remove(); //清除css
                //console.log('destroy js', page_info.flag+'_'+page_info.name)

                eval(page_info.flag+'_'+page_info.name+"=null"); //清除js
             }
        }

        var k;
        for (k in this.pages) {
            if(!this.pages[k].settings.lazy){
                load_template(k);
            }
        }

        this.new_page = function(name, para, animation){
            if(name instanceof Array){
                para = name[1];
                animation = name[2];
                name = name[0];
            }

            if(name.startsWith(".")){
                name = this.current.page_name + name
            }
            console.log('打开新页面 ',name);

            name = name.replace(/\./g, '_')
            var page=self.pages[name];

            if(page.load === null){
                console.log('load_page', name);
                load_template(name, this.new_page, [name, para, animation]);
                return
            }
            if(self.lock)return
            self.lock = true

            var flag = name;
            if(!page.settings.cache){
                flag = 'cd'+(new Date()).valueOf();
            }
            var page_id = 'page_'+flag;
            var page_info;

            if(animation===undefined){animation = page.settings.animation}

            if(page.settings.cache && page_id in self.page_cache){
                //有缓存
                console.log("缓存 "+page_id);
                self.history.unshift(page_id);
                page_info = self.page_cache[page_id];

            }else{
                page_id = 'page_'+flag;

                page_info = {
                    'name':name,
                    'page': page,
                    'flag': flag,
                    'page_id': page_id,
                    'app': null,
                };
                self.page_cache[page_id]=page_info;
                self.history.unshift(page_id);


                $("head").append('<style id="'+page_id+'_style">'+page.css+'</style>'); //添加css

                //self.app.append('<div id='+page_id+' class="pageX '+animation+'">'+page.html+'</div>');//添加dom
                self.app.append('<div id='+page_id+' class="pageX">'+page.html+'</div>');//添加dom

                page_info.app = Compiler.js(page.js, name, flag, page_id, page.root_name);

                if(page_info.app.onCreate){
                    page_info.app.onCreate(page_info.app, page_id, para);
                }
            }
           self.current = page_info.app;
           self.current.page_name = name;

            $("#"+page_id).css('z-index', self.history.length);
            page_info.animation = animation;
            //console.log('打开新页面 '+name+' '+self.history.length+' '+page_id);

            //动画
            if(animation && self.history.length>1){
                var page_dom = document.getElementById(page_id);
                function animationEndFn(){
                    console.log("show "+page_id);
                    if(page_info.app.onShow){
                        page_info.app.onShow(page_info.app, para);
                    }
                    page_dom.removeEventListener("webkitAnimationEnd", animationEndFn)
                    self.lock = false
                }
                page_dom.addEventListener("webkitAnimationEnd", animationEndFn)
                $("#"+page_id).css('animation', 'pageXShow'+animation+' 0.8s');

            }else{
                switch(animation){
                    case 'top':
                        $("#"+page_id).css('top', 0);
                        break
                    case 'right':
                    default:
                        $("#"+page_id).css('left', 0);
                }
                $(".pageX.reset-page-cover").remove()
                if(page_info.app.onShow){
                    page_info.app.onShow(page_info.app, para);
                }
                self.lock = false
            }
            return page_id;

        }

        this.go_back = function(page_tag, animation){
            //page_tag = page_id or page_name
            if(self.history.length==1){
                return
            }
            console.log('go_back', page_tag)

            var _page_id = self.history.shift()
            var _page_info = self.page_cache[_page_id];
            if(page_tag!=undefined){
                page_tag = page_tag.replace(/\./g, '_')
                if(_page_id!=page_tag || _page_info.page.name!=page_tag){
                    while(self.history.length>1){
                        var _id = self.history[0];
                        var _info = self.page_cache[_id];
                        if( _id===page_tag || _info.page.name==page_tag)break;

                        self.history.shift();
                        destroy_page(_id);
                    }
                }

            }

            if(animation===undefined){animation =_page_info.animation}

            var page_dom = document.getElementById(_page_id);

            if(_page_info.app.onPause){
                _page_info.app.onPause(_page_info.app);
            }

            var page_show_info  = self.page_cache[self.history[0]];
            self.current = page_show_info.app;

            if(animation){
                page_dom.addEventListener("webkitAnimationEnd", function() {
                    destroy_page(_page_id);
                    //var page_show_info  = self.page_cache[self.history[0]];
                    if(page_show_info.app.onShow){
                        page_show_info.app.onShow(page_show_info.app);
                    }
                })
                $(page_dom).css('animation', 'pageXBack'+animation+' 0.8s');

            }else{
                switch(animation){
                    case 'top':
                        $("#"+_page_id).css('top', '-100%');
                        break
                    case 'right':
                        $("#"+_page_id).css('left', '-100%');
                        break
                    default:
                        $("#"+_page_id).css('left', "100%");
                }
                destroy_page(_page_id);
                if(page_show_info.app.onShow){
                    page_show_info.app.onShow(page_show_info.app);
                }
            }
        }
        this.reset_page=function(page_name, para){
            //重置页面, 无动画
            console.log("重置", page_name)
            page_name = page_name.replace(/\./g, '_')

            var page_id = self.history[0];
            var page_info = self.page_cache[page_id];
            if(page_info.name==page_name){return}
            self.history.shift();
            destroy_page(page_id);
            self.app.append('<div class="pageX reset-page-cover" style="background-color:#fff;z-index:'+self.history.length+'"></div>');//添加dom
            self.new_page(page_name, para, null);
        }

        FnQueue.call(function () {
            //console.log("pages"+JSON.stringify(self.pages))
            self.settings.onLoaded();
        }, null, 'onLoaded')

    }
})();
