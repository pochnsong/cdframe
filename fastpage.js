/***
 * cdframe 单页面应用框架
 *
 * 依赖jquery.js
 *
 *
 */

function FastListPage(page_id, list_url) {

    return {
       el: '#'+page_id,
       data: {
           object_list: [],
           object_count: 0,
           filters:{
               limit: 20,
               skip: 0,
           },
       },
       methods:{
           get_more(){
               if(this.filters.skip>this.object_count){
                   return
               }
               this.filters.skip += this.filters.limit;
               this.get_list()
           },
           get_filters(){
               return this.filters;
           },
           get_list(reload){
               var _this = this;
               if(reload){
                   this.filters.skip = 0;
                   this.object_list = []
               }
               Host.get(list_url, this.get_filters(), function(data){
                   console.log(System.copy(data))
                   if(data.status == "OK"){
                       _this.object_list = _this.object_list.concat(data.object_list);
                       if(data.count != undefined){
                           _this.object_count = data.count
                       }
                   }else{
                        alert(JSON.stringify(data))
                   }
               })
           },
       }
    }
}
